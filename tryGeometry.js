const { Beam, Tringular, Cone } = require("./geometry");

// Create Object
let beamOne = new Beam(6, 7, 8);
let tringularOne = new Tringular(5, 10, 10);
let coneOne = new Cone(4, 5, 6);

// Calculate Volume
let a = beamOne.calculateVolume();
console.log(a);
let b = tringularOne.calculateVolume();
console.log(b);
let c = coneOne.calculateVolume();
console.log(c);
let d = a + b + c;
console.log(`Addition from 3 Volume.`);
console.log(d);

// Calculate Surface
let e = beamOne.calculateSurface();
console.log(e);
let f = tringularOne.calculateSurface();
console.log(f);
let g = coneOne.calculateSurface();
console.log(g);
let h = e + f + g;
console.log(`Addition from 3 Surface.`);
console.log(h);
