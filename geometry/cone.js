const threeDemention = require("./threeDimention");

class Cone extends threeDemention {
  constructor(radius, width, height) {
    // New Three Demention
    super("Cone");
    // const phi = 3.14;

    this.radius = radius;
    this.width = width;
    this.height = height;
  }

  // Overriding
  calculateVolume() {
    //   Call Function in Parent Class
    const phi = 3.14;
    super.calculateVolume();

    return (phi * this.radius ** 2 * this.height) / 3;
  }

  //   Overriding
  calculateSurface() {
    //   Call Function in Parent Class
    const phi = 3.14;
    super.calculateSurface();

    return phi * this.radius * (this.width + this.radius);
  }
}

module.exports = Cone;
