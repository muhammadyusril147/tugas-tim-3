const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "Three Dimention");
    // new Geometry('Square', 'Three Dimention')

    // Make abstract
    if (this.constructor === ThreeDimention) {
      throw new Error("Cannot instantiate from Abstract Class"); // Because it's abstract
    }
  }

  calculateVolume() {
    console.log(`${this.name} is calculating volume!`);
  }

  calculateSurface() {
    console.log(`${this.name} is calculating surface!`);
  }
}

module.exports = ThreeDimention;
