const threeDemention = require("./threeDimention");

class Tringular extends threeDemention {
  constructor(base, baseHeight, limasHeight) {
    // New Three Demention
    super("Tringular Pyramid");

    this.base = base;
    this.baseHeight = baseHeight;
    this.limasHeight = limasHeight;
  }

  // Overriding
  calculateVolume() {
    //   Call Function in Parent Class
    super.calculateVolume();

    return (((1 / 3) * 1) / 2) * this.base * this.baseHeight * this.limasHeight;
  }

  //   Overriding
  calculateSurface() {
    //   Call Function in Parent Class
    super.calculateSurface();

    return (
      0.5 * this.base * this.baseHeight + 0.5 * this.base * this.limasHeight * 3
    );
  }
}

module.exports = Tringular;
