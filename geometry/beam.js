const ThreeDimention = require(`./threeDimention`);

class Beam extends ThreeDimention {
  constructor(width, length, height) {
    super("Beam");
    this.width = width;
    this.length = length;
    this.height = height;
  }

  calculateVolume() {
    super.calculateVolume();
    return this.width * this.length * this.height;
  }
  calculateSurface() {
    super.calculateSurface();
    return (this.width + this.length + this.height) * 3;
  }
}

module.exports = Beam;
